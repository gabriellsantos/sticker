json.extract! picture, :id, :name, :photo, :price, :category_id, :device_id, :created_at, :updated_at
json.url picture_url(picture, format: :json)
