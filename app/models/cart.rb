# == Schema Information
#
# Table name: carts
#
#  id         :integer          not null, primary key
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Cart < ActiveRecord::Base

  has_many :cart_items

  extend Enumerize
  enumerize :stats, in: [:aberto, :concluido, :cancelado], predicates: true, default: :aberto

  def total
    cart_items.sum(:price)
  end
end
