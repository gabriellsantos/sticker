class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.references :picture, index: true, foreign_key: true
      t.references :cart, index: true, foreign_key: true
      t.decimal :price
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
