# == Schema Information
#
# Table name: cart_items
#
#  id         :integer          not null, primary key
#  picture_id :integer
#  cart_id    :integer
#  price      :decimal(, )
#  quantity   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_cart_items_on_cart_id     (cart_id)
#  index_cart_items_on_picture_id  (picture_id)
#
# Foreign Keys
#
#  fk_rails_...  (cart_id => carts.id)
#  fk_rails_...  (picture_id => pictures.id)
#

class CartItem < ActiveRecord::Base
  belongs_to :picture
  belongs_to :cart
end
