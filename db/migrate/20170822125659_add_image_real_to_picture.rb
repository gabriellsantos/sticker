class AddImageRealToPicture < ActiveRecord::Migration
  def change
    add_column :pictures, :image_real, :string
  end
end
