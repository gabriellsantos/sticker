class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.string :name
      t.string :photo
      t.decimal :price
      t.references :category, foreign_key: true
      t.references :device, foreign_key: true

      t.timestamps
    end
  end
end
