json.extract! device, :id, :name, :photo, :maker_id, :created_at, :updated_at
json.url device_url(device, format: :json)
