class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|
      t.string :name
      t.string :photo
      t.references :maker, foreign_key: true

      t.timestamps
    end
  end
end
