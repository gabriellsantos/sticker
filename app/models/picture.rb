# == Schema Information
#
# Table name: pictures
#
#  id          :integer          not null, primary key
#  name        :string
#  photo       :string
#  price       :decimal(, )
#  category_id :integer
#  device_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  image_real  :string
#
# Indexes
#
#  index_pictures_on_category_id  (category_id)
#  index_pictures_on_device_id    (device_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#  fk_rails_...  (device_id => devices.id)
#

class Picture < ActiveRecord::Base
  belongs_to :category
  belongs_to :device

  validates :name, :photo, :price, :category_id, :device_id, presence: true

  mount_uploader :photo, AvatarUploader
  mount_uploader :image_real, AvatarUploader

  def to_s
    self.name
  end

end
