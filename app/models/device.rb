# == Schema Information
#
# Table name: devices
#
#  id         :integer          not null, primary key
#  name       :string
#  photo      :string
#  maker_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_devices_on_maker_id  (maker_id)
#
# Foreign Keys
#
#  fk_rails_...  (maker_id => makers.id)
#

class Device < ActiveRecord::Base
  belongs_to :maker
  has_many :pictures
  has_many :categories, -> {distinct}, through: :pictures

  validates :name, :photo, :maker_id, presence: true

  mount_uploader :photo, AvatarUploader

  def to_s
    self.name
  end

end
