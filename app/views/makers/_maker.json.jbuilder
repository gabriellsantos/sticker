json.extract! maker, :id, :name, :logo, :created_at, :updated_at
json.url maker_url(maker, format: :json)
