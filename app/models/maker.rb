# == Schema Information
#
# Table name: makers
#
#  id         :integer          not null, primary key
#  name       :string
#  logo       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Maker < ActiveRecord::Base

  has_many :devices
  mount_uploader :logo, AvatarUploader


  def to_s
    self.name
  end
end
